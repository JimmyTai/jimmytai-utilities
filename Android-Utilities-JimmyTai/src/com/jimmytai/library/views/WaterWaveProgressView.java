package com.jimmytai.library.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.jimmytai.library.utilities.R;
import com.jimmytai.library.utils.ScreenUtils;

/**
 * Created by JimmyTai on 15/10/21.
 */
public class WaterWaveProgressView extends View {

    private static final String TAG = "WaterWaveProgressView";

    private static final int DEFAULT_SIZE = 50;
    private static final int DEFAULT_FRAME_WIDTH = 2;
    private static final float DEFAULT_PROGRESS_MAX = 100;
    private static final float DEFAULT_PROGRESS = 50;
    private static final float DEFAULT_WAVE_RADIUS_RATIO = 0.05f;
    private static final int DEFAULT_FRAME_COLOR = Color.parseColor("#33333333");
    private static final int DEFAULT_PROGRESS_COLOR = Color.parseColor("#77CCCC88");
    private static final int DEFAULT_WAVE_SPEED = 100;
    private static final boolean DEFAULT_SHOW_PERCENTAGE = false;
    private static final int DEFAULT_TEXT_COLOR = Color.parseColor("#656565");

    private Context context;

    /**
     * 邊框寬度
     */
    private float FRAME_WIDTH;
    /**
     * 當前進度與最大進度
     */
    private float PROGRESS_MAX, PROGRESS;
    /**
     * 波紋振幅與半徑之比。(建議設置：<0.1)
     */
    private float A;
    /**
     * 邊框顏色
     */
    private int FRAME_COlOR;
    /**
     * 進度顏色
     */
    private int PROGRESS_COLOR;
    /**
     * 波浪速度
     */
    private int WAVE_SPEED;
    /**
     * 顯示百分比
     */
    private boolean SHOW_PERCENTAGE;
    /**
     * 百分比文字大小
     */
    private float TEXT_SIZE;
    /**
     * 百分比文字顏色
     */
    private int TEXT_COLOR;

    /**
     * 繪製波浪的筆
     */
    private Paint progressPaint;


    /**
     * 繪製文字的筆
     */
    private Paint textPaint;

    /**
     * 繪製邊框的筆
     */
    private Paint framePaint;


    private Rect textBounds = new Rect();

    /**
     * 圆弧圆心位置
     */
    private float centerX, centerY;

    /**
     * 内圆所在的矩形
     */
    private RectF circleRectF;

    public WaterWaveProgressView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public WaterWaveProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public WaterWaveProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    private void init() {
        this.FRAME_WIDTH = (int) ScreenUtils.dip2px(context, DEFAULT_FRAME_WIDTH);
        this.PROGRESS_MAX = DEFAULT_PROGRESS_MAX;
        this.PROGRESS = DEFAULT_PROGRESS;
        this.A = DEFAULT_WAVE_RADIUS_RATIO;
        this.FRAME_COlOR = DEFAULT_FRAME_COLOR;
        this.PROGRESS_COLOR = DEFAULT_PROGRESS_COLOR;
        this.WAVE_SPEED = DEFAULT_WAVE_SPEED;
        this.SHOW_PERCENTAGE = DEFAULT_SHOW_PERCENTAGE;
        this.TEXT_COLOR = DEFAULT_TEXT_COLOR;

        circleRectF = new RectF();

        progressPaint = new Paint();
        progressPaint.setColor(this.PROGRESS_COLOR);
        progressPaint.setAntiAlias(true);

        textPaint = new Paint();
        textPaint.setColor(this.TEXT_COLOR);
        textPaint.setAntiAlias(true);

        framePaint = new Paint();
        framePaint.setStyle(Paint.Style.STROKE);
        framePaint.setAntiAlias(true);
        framePaint.setColor(this.FRAME_COlOR);
        autoRefresh();
    }

    private void init(AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WaterWaveProgressView,
                0, 0);
        try {
            this.FRAME_WIDTH = a.getDimension(R.styleable.WaterWaveProgressView_frameWidth, ScreenUtils
                    .dip2px(context,
                            DEFAULT_FRAME_WIDTH));
            this.PROGRESS_MAX = a.getFloat(R.styleable.WaterWaveProgressView_maxProgress,
                    DEFAULT_PROGRESS_MAX);
            this.PROGRESS = a.getFloat(R.styleable.WaterWaveProgressView_progress, DEFAULT_PROGRESS);
            this.A = a.getFloat(R.styleable.WaterWaveProgressView_waveRadiusRatio, DEFAULT_WAVE_RADIUS_RATIO);
            this.FRAME_COlOR = a.getColor(R.styleable.WaterWaveProgressView_frameColor,
                    DEFAULT_FRAME_COLOR);
            this.PROGRESS_COLOR = a.getColor(R.styleable.WaterWaveProgressView_progressColor,
                    DEFAULT_PROGRESS_COLOR);
            this.WAVE_SPEED = a.getInt(R.styleable.WaterWaveProgressView_waveSpeed, DEFAULT_WAVE_SPEED);
            this.SHOW_PERCENTAGE = a.getBoolean(R.styleable.WaterWaveProgressView_showPercentage,
                    DEFAULT_SHOW_PERCENTAGE);
            this.TEXT_SIZE = a.getDimension(R.styleable.WaterWaveProgressView_percentageTextSize, 0f);
            this.TEXT_COLOR = a.getColor(R.styleable.WaterWaveProgressView_percentageTextColor,
                    DEFAULT_TEXT_COLOR);
        } finally {
            a.recycle();
        }

        circleRectF = new RectF();

        progressPaint = new Paint();
        progressPaint.setColor(this.PROGRESS_COLOR);
        progressPaint.setAntiAlias(true);

        textPaint = new Paint();
        textPaint.setColor(this.TEXT_COLOR);
        textPaint.setAntiAlias(true);

        framePaint = new Paint();
        framePaint.setStyle(Paint.Style.STROKE);
        framePaint.setAntiAlias(true);
        framePaint.setColor(this.FRAME_COlOR);

        autoRefresh();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = measureDimension((int) ScreenUtils.dip2px(context, DEFAULT_SIZE), widthMeasureSpec);
        int height = measureDimension((int) ScreenUtils.dip2px(context, DEFAULT_SIZE), heightMeasureSpec);

        setMeasuredDimension(width, height);

        // 計算圓半徑和圓心
        // >> 1 向右移一個bit 相當於除以2
        int circleRadius = Math.min(width, height) >> 1;
        Log.d(TAG, "width: " + width + ", height: " + height + ", radius" + circleRadius);
        framePaint.setStrokeWidth(FRAME_WIDTH);
        centerX = width / 2;
        centerY = height / 2;

        validRadius = circleRadius - FRAME_WIDTH;
        radiansPerX = (float) (Math.PI / validRadius);

        // 內圓所在的矩形
        circleRectF.set(centerX - validRadius, centerY - validRadius,
                centerX + validRadius, centerY + validRadius);
        if (this.TEXT_SIZE == 0) {
            this.TEXT_SIZE = (int) validRadius / 4;
        }
    }

    protected int measureDimension(int defaultSize, int measureSpec) {
        int result;

        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // 此View尺寸給一定值或設為match parent && 父控件尺寸有一定值
            result = specSize;
        } else if (specMode == MeasureSpec.AT_MOST) {
            // 1. 此View設為wrap content 或設為 match parent && 父控件有尺寸限制
            result = Math.min(defaultSize, specSize);
        } else {
            // 此View尺寸不為定值 && 父控件沒有限制
            result = defaultSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // 繪製邊框
        canvas.drawCircle(centerX, centerY, validRadius + (FRAME_WIDTH / 2), framePaint);
        // 繪製水波
        canvas.drawPath(getWavePath(xOffset), progressPaint);

        if (SHOW_PERCENTAGE) {
            // 繪製文字
            textPaint.setTextSize(ScreenUtils.dip2px(context, this.TEXT_SIZE));
            String strProgress = String.valueOf((int) PROGRESS) + "%";
            // 測量文字寬度
            float w1 = textPaint.measureText(strProgress);
            // 測量文字高度
            textPaint.getTextBounds("8", 0, 1, textBounds);
            float h1 = textBounds.height();
            // 補一小差距
            float extraW = textPaint.measureText("%") / 7;
            canvas.drawText(strProgress, centerX - w1 / 2 + extraW, centerY + h1 / 2, textPaint);
        }
    }

    /**
     * 繪製水波的路徑
     */
    private Path wavePath;

    /**
     * 每一個像素對應的弧度數
     */
    private float radiansPerX;

    /**
     * 內圓半徑
     **/
    private float validRadius;

    /**
     * x方向偏移量
     **/
    private float xOffset;

    /**
     * 獲取水波曲線（包含圓弧部分）的路徑
     *
     * @param xOffset x方向像素偏移量
     */
    private Path getWavePath(float xOffset) {
        if (wavePath == null) {
            wavePath = new Path();
        } else {
            wavePath.reset();
        }

        float[] startPoint = new float[2];  // 波浪起點
        float[] endPoint = new float[2];  // 波浪終點

        for (int i = 0; i <= validRadius * 2; i += 2) {
            float x = centerX - validRadius + i;
            float y = (float) (centerY + validRadius * (1.0f + A) * 2 * (0.5f - PROGRESS / PROGRESS_MAX)
                    + validRadius * A * Math.sin((xOffset + i) * radiansPerX));

            // 計算內圓內部的點，邊框上的忽略
            if (calDistance(x, y, centerX, centerY) > validRadius) {
                if (x < centerX) {
                    continue;  // 左邊框，繼續循環
                } else {
                    break; // 右邊框，結束循環
                }
            }

            // 第一個點
            if (wavePath.isEmpty()) {
                startPoint[0] = x;
                startPoint[1] = y;
                wavePath.moveTo(x, y);
            } else {
                wavePath.lineTo(x, y);
            }

            endPoint[0] = x;
            endPoint[1] = y;
        }

        if (wavePath.isEmpty()) {
            if (PROGRESS / PROGRESS_MAX >= 0.5f) {
                // 满格
                wavePath.moveTo(centerX, centerY - validRadius);
                wavePath.addCircle(centerX, centerY, validRadius, Path.Direction.CW);
            } else {
                // 空格
                return wavePath;
            }
        } else {
            // 添加圆弧部分
            float startDegree = calDegreeByPosition(startPoint[0], startPoint[1]);  //0~180
            float endDegree = calDegreeByPosition(endPoint[0], endPoint[1]); //180~360
            wavePath.arcTo(circleRectF, endDegree - 360, startDegree - (endDegree - 360));
        }

        return wavePath;
    }

    private float calDistance(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    // 根據當前位置，計算出進度條已經轉過的角度
    private float calDegreeByPosition(float currentX, float currentY) {
        float a1 = (float) (Math.atan(1.0f * (centerX - currentX) / (currentY - centerY)) / Math.PI * 180);
        if (currentY < centerY) {
            a1 += 180;
        } else if (currentY > centerY && currentX > centerX) {
            a1 += 360;
        }

        return a1 + 90;
    }

    /**
     * 設置進度最大值
     *
     * @param max 進度最大 (float)
     */
    public void setMaxProgress(float max) {
        this.PROGRESS_MAX = max;
        invalidate();
    }

    /**
     * 取得進度最大值
     *
     * @return 進度最大值 (float)
     */
    public float getMaxProgress() {
        return this.PROGRESS_MAX;
    }

    /**
     * 設置水波進度
     *
     * @param progress 進度 (float)
     */
    public void setProgress(float progress) {
        this.PROGRESS = progress;
        invalidate();
    }

    /**
     * 取得水波目前進度
     *
     * @return 進度 (float)
     */
    public float getProgress() {
        return this.PROGRESS;
    }

    /**
     * 設定邊框尺寸 (dp)
     *
     * @param size 單位dp
     */
    public void setFrameWidth(int size) {
        this.FRAME_WIDTH = ScreenUtils.dip2px(context, size);
        invalidate();
    }

    /**
     * 取得邊框尺寸 (dp)
     *
     * @return 邊框寬度 單位dp
     */
    public int getFrameWidth() {
        return (int) ScreenUtils.px2dip(context, this.FRAME_WIDTH);
    }

    /**
     * 設置波紋振幅與半徑之比 (請設置：<0.1)
     *
     * @param ratio 波紋振幅半徑之比
     */
    public void setWaveRadiusRatio(float ratio) {
        if (ratio > 0.1f) {
            this.A = 0.1f;
        } else {
            this.A = ratio;
        }
        invalidate();
    }

    /**
     * 取得波紋振幅與半徑之比
     *
     * @return float
     */
    public float getWaveRadiusRatio() {
        return this.A;
    }

    /**
     * 設置邊框顏色
     *
     * @param color 預設色票 #33333333
     */
    public void setFrameColor(int color) {
        this.FRAME_COlOR = color;
        framePaint.setColor(this.FRAME_COlOR);
        invalidate();
    }

    /**
     * 取得邊框顏色
     *
     * @return 邊框顏色 (Integer)
     */
    public int getFrameColor() {
        return FRAME_COlOR;
    }

    /**
     * 設置水波顏色
     *
     * @param color 預設色票 #77CCCC88
     */
    public void setProgressColor(int color) {
        this.PROGRESS_COLOR = color;
        progressPaint.setColor(PROGRESS_COLOR);
        invalidate();
    }

    /**
     * 取得水波顏色
     *
     * @return 水波顏色 (Integer)
     */
    public int getProgressColor() {
        return PROGRESS_COLOR;
    }

    /**
     * 設置水波紋速度
     *
     * @param speed millisecond
     */
    public void setWaveSpeed(int speed) {
        this.WAVE_SPEED = speed;
        invalidate();
    }

    /**
     * 取得水波紋速度
     *
     * @return 波浪速度 millisecond
     */
    public int getWaveSpeed() {
        return this.WAVE_SPEED;
    }

    /**
     * 設置百分比文字大小 (dp)
     *
     * @param size 文字大小 (dp)
     */
    public void setPercentageTextSize(int size) {
        this.TEXT_SIZE = ScreenUtils.dip2px(context, size);
        invalidate();
    }

    /**
     * 取得百分比文字大小 (dp)
     *
     * @return 文字大小 (dp)
     */
    public int getPercentageTextSize() {
        return (int) ScreenUtils.px2dip(context, this.TEXT_SIZE);
    }

    /**
     * 設置百分比文字顏色
     *
     * @param color 顏色：預設色票 #656565
     */
    public void setPercentageTextColor(int color) {
        this.TEXT_COLOR = color;
        textPaint.setColor(this.TEXT_COLOR);
        invalidate();
    }

    /**
     * 取得百分比文字顏色
     *
     * @return 文字顏色 (Integer)
     */
    public int getPercentageTextColor() {
        return this.TEXT_COLOR;
    }

    /**
     * 設置是否顯示百分比
     *
     * @param enable false: 關閉; true: 開啟
     */
    public void showPercentage(boolean enable) {
        this.SHOW_PERCENTAGE = enable;
        invalidate();
    }

    /**
     * 取得是否顯示百分比
     *
     * @return false: 關閉; true: 開啟
     */
    public boolean getIsShowPercentage() {
        return this.SHOW_PERCENTAGE;
    }

    /**
     * 自动刷新页面，创造水波效果。组件销毁后该线城将自动停止。
     */
    private void autoRefresh() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!detached) {
                    xOffset += (validRadius / 16f);
                    SystemClock.sleep(WAVE_SPEED);
                    // 可以在執行緒內直接更新UI
                    postInvalidate();
                }
            }
        }).start();
    }

    // 標記View是否已經銷毀
    private boolean detached = false;

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        detached = true;
    }
}

package com.jimmytai.library.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.jimmytai.library.utilities.R;
import com.jimmytai.library.utils.ScreenUtils;

/**
 * Created by JimmyTai on 15/10/29.
 */
public class SectorProgressView extends View {

    private static final String TAG = "SectorProgressView";
    public static boolean DEBUG = false;

    private Context context;

    private static final int DEFAULT_BACKGROUND_COLOR = Color.parseColor("#eceff1");
    private static final int DEFAULT_PROGRESS_COLOR = Color.parseColor("#ef9a9a");
    private static final float DEFAULT_START_ANGLE = 270;
    private static final float DEFAULT_PROGRESS = 0;
    private static final float DEFAULT_MAX_PROGRESS = 100;
    private static final float DEFAULT_SIZE = 50;

    private int backgroundColor, progressColor;
    private float startAngle;
    private float startAngleBg, startAnglePg, lengthAngleBg, lengthAnglePg;
    private float progress;
    private float progressMax;

    private int centerX;
    private int centerY;

    private Paint bgPaint, pgPaint;
    private RectF oval;

    public SectorProgressView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public SectorProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public SectorProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    private void init() {
        this.backgroundColor = DEFAULT_BACKGROUND_COLOR;
        this.progressColor = DEFAULT_PROGRESS_COLOR;
        this.progress = DEFAULT_PROGRESS;
        this.progressMax = DEFAULT_MAX_PROGRESS;
        this.startAngle = DEFAULT_START_ANGLE;

        computeArcArgs(this.startAngle, this.progressMax, this.progress);

        bgPaint = new Paint();
        bgPaint.setColor(backgroundColor);
        bgPaint.setAntiAlias(true);
        pgPaint = new Paint();
        pgPaint.setColor(progressColor);
        pgPaint.setAntiAlias(true);
    }

    private void init(AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SectorProgressView, 0, 0);
        try {
            this.backgroundColor = a.getColor(R.styleable.SectorProgressView_sectorBgColor,
                    DEFAULT_BACKGROUND_COLOR);
            this.progressColor = a.getColor(R.styleable.SectorProgressView_sectorPgColor,
                    DEFAULT_PROGRESS_COLOR);
            this.progress = a.getFloat(R.styleable.SectorProgressView_sectorProgress, DEFAULT_PROGRESS);
            this.progressMax = a.getFloat(R.styleable.SectorProgressView_sectorProgressMax,
                    DEFAULT_MAX_PROGRESS);
            this.startAngle = a.getFloat(R.styleable.SectorProgressView_sectorStartAngle,
                    DEFAULT_START_ANGLE);
            computeArcArgs(this.startAngle, this.progressMax, this.progress);
        } finally {
            a.recycle();
        }
        bgPaint = new Paint();
        bgPaint.setColor(backgroundColor);
        bgPaint.setAntiAlias(true);
        pgPaint = new Paint();
        pgPaint.setColor(progressColor);
        pgPaint.setAntiAlias(true);
    }

    private void computeArcArgs(float startAngle, float progressMax, float progress) {
        this.lengthAnglePg = (progress / progressMax) * 360;
        this.lengthAngleBg = 360 - lengthAnglePg;
        this.startAnglePg = startAngle;
        this.startAngleBg = startAnglePg + lengthAnglePg;
        oval = new RectF();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = measureDimension((int) ScreenUtils.dip2px(this.context, DEFAULT_SIZE), widthMeasureSpec);
        int height = measureDimension((int) ScreenUtils.dip2px(this.context, DEFAULT_SIZE),
                heightMeasureSpec);

        setMeasuredDimension(width, height);

        int circleRadius = Math.min(width, height) >> 1;

        centerX = width / 2;
        centerY = height / 2;

        oval.set(centerX - circleRadius, centerY - circleRadius, centerX + circleRadius, centerY +
                circleRadius);
    }

    protected int measureDimension(int defaultSize, int measureSpec) {
        int result;

        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // 此View尺寸給一定值或設為match parent && 父控件尺寸有一定值
            result = specSize;
        } else if (specMode == MeasureSpec.AT_MOST) {
            // 1. 此View設為wrap content 或設為 match parent && 父控件有尺寸限制
            result = Math.min(defaultSize, specSize);
        } else {
            // 此View尺寸不為定值 && 父控件沒有限制
            result = defaultSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawArc(oval, startAngleBg, lengthAngleBg, true, bgPaint);
        canvas.drawArc(oval, startAnglePg, lengthAnglePg, true, pgPaint);
    }

    private void refreshTheLayout() {
        invalidate();
        requestLayout();
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        bgPaint.setColor(backgroundColor);
        refreshTheLayout();
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
        pgPaint.setColor(progressColor);
        refreshTheLayout();
    }

    public float getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(float startAngle) {
        this.startAngle = startAngle;
        computeArcArgs(startAngle, this.progressMax, this.progress);
        refreshTheLayout();
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
        computeArcArgs(this.startAngle, this.progressMax, progress);
        refreshTheLayout();
    }

    public float getProgressMax() {
        return progressMax;
    }

    public void setProgressMax(float progressMax) {
        this.progressMax = progressMax;
        computeArcArgs(this.startAngle, progressMax, this.progress);
        refreshTheLayout();
    }
}

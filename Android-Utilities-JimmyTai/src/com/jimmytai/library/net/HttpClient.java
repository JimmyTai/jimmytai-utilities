package com.jimmytai.library.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.jimmytai.library.log.JLog;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

public class HttpClient {

    public static final String TAG = "HttpClient";
    public static boolean DEBUG = false;

    private static final int DEFAULT_CONNECTION_TIMEOUT = 800;

    /**
     * Get all network state
     *
     * @param context your application context
     * @return whether network is available
     */
    public static boolean isNetworkAvailable(Context context) {

        JLog.i(DEBUG, TAG, "isNetworkAvailable()");

        ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();

        if (networkInfo != null) {
            if (networkInfo.isConnected()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get Wifi network state
     *
     * @param context your application context
     * @return whether wifi is available
     */
    public static boolean isWifiNetworkAvailable(Context context) {

        JLog.i(DEBUG, TAG, "isWifiNetworkAvailable()");

        ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetworkInfo = mConnectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiNetworkInfo != null && wifiNetworkInfo.isAvailable()) {
            if (wifiNetworkInfo.isConnected()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get mobile network state
     *
     * @param context your application context
     * @return whether mobile network is available
     */
    public static boolean isMobileNetworkAvailable(Context context) {

        JLog.i(DEBUG, TAG, "isMobileNetworkAvailable()");

        ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileNetworkInfo = mConnectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mobileNetworkInfo != null && mobileNetworkInfo.isAvailable()) {
            if (mobileNetworkInfo.isConnected()) {
                return true;
            }
        }

        return false;
    }


    /**
     * Connect to a web and get the content by HTTP GET Method
     *
     * @param strUrl                  url of the website
     * @param readTimeoutMillis       the timeout value of reading
     * @param connectionTimeoutMillis the timeout value of connection
     * @return website content
     * @throws TimeoutException         HTTP connection or read timeout
     * @throws BadResponseCodeException HTTP response code is not 200
     */
    public static String httpGet(String strUrl, int readTimeoutMillis, int connectionTimeoutMillis) throws
            TimeoutException,
            BadResponseCodeException {

        JLog.i(DEBUG, TAG, "httpGet()");

        URL url;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            url = new URL(strUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(readTimeoutMillis);
            httpURLConnection.setConnectTimeout(connectionTimeoutMillis);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.setRequestProperty("connection", "Keep-Alive");

            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                byteArrayOutputStream = new ByteArrayOutputStream();
                int len;
                byte[] buffer = new byte[1000];
                while ((len = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, len);
                }
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toString();
            } else {
                throw new BadResponseCodeException(httpURLConnection.getResponseCode(), "ResponseCode is " +
                        "not 200");
            }
        } catch (SocketTimeoutException e) {
            throw new TimeoutException("Connection or read timeout");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (byteArrayOutputStream != null)
                    byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return null;
    }

    /**
     * Connect to a web and get the content by HTTP GET Method
     *
     * @param strUrl            url of the website
     * @param readTimeoutMillis the timeout value of reading
     * @return website content
     * @throws TimeoutException         HTTP connection or read timeout
     * @throws BadResponseCodeException HTTP response code is not 200
     */
    public static String httpGet(String strUrl, int readTimeoutMillis) throws TimeoutException,
            BadResponseCodeException {

        JLog.i(DEBUG, TAG, "httpGet()");

        URL url;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            url = new URL(strUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(readTimeoutMillis);
            httpURLConnection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.setRequestProperty("connection", "Keep-Alive");

            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                byteArrayOutputStream = new ByteArrayOutputStream();
                int len;
                byte[] buffer = new byte[1000];
                while ((len = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, len);
                }
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toString();
            } else {
                throw new BadResponseCodeException(httpURLConnection.getResponseCode(), "ResponseCode is " +
                        "not 200");
            }
        } catch (SocketTimeoutException e) {
            throw new TimeoutException("Connection or read timeout");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (byteArrayOutputStream != null)
                    byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return null;
    }

    /**
     * Connect to a web and get the content by HTTP GET Method
     *
     * @param strUrl url of the website
     * @return website content
     * @throws TimeoutException         HTTP connection or read timeout
     * @throws BadResponseCodeException HTTP response code is not 200
     */
    public static String httpGet(String strUrl) throws TimeoutException, BadResponseCodeException {

        JLog.i(DEBUG, TAG, "httpGet()");

        URL url;
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            url = new URL(strUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.setRequestProperty("connection", "Keep-Alive");

            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                byteArrayOutputStream = new ByteArrayOutputStream();
                int len;
                byte[] buffer = new byte[1000];
                while ((len = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, len);
                }
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toString();
            } else {
                throw new BadResponseCodeException(httpURLConnection.getResponseCode(), "ResponseCode is " +
                        "not 200");
            }
        } catch (SocketTimeoutException e) {
            throw new TimeoutException("Connection or read timeout");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (byteArrayOutputStream != null)
                    byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return null;
    }

    /**
     * Connect to a web and get the content by HTTP POST Method
     *
     * @param strUrl                  url of the website
     * @param itemList                the ArrayList of params you want to upload (*PostItem)
     * @param readTimeoutMillis       the timeout value of reading
     * @param connectionTimeoutMillis the timeout value of connection
     * @return website content
     * @throws TimeoutException         HTTP connection or read timeout
     * @throws BadResponseCodeException HTTP response code is not 200
     */
    public static String httpPost(String strUrl, ArrayList<PostItem> itemList, int readTimeoutMillis, int
            connectionTimeoutMillis) throws TimeoutException, BadResponseCodeException {

        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;
        String response = null;

        try {
            URL url = new URL(strUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.setRequestProperty("connection", "Keep-Alive");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("charset", "utf-8");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(connectionTimeoutMillis);
            httpURLConnection.setReadTimeout(readTimeoutMillis);

            String params = null;

            if (httpURLConnection.getResponseCode() != 200) {

                for (int i = 0; i < itemList.size(); i++) {
                    params += itemList.get(i).getKey() + "=" + itemList.get(i).getValue();
                    if (i < itemList.size() - 1) {
                        params += "&";
                    }
                }

                if (params != null && !params.trim().equals("")) {
                    printWriter = new PrintWriter(httpURLConnection.getOutputStream());
                    printWriter.print(params);
                    printWriter.flush();
                }

                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream
                        ()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }
            } else {
                throw new BadResponseCodeException(httpURLConnection.getResponseCode(), "ResponseCode is " +
                        "not 200");
            }
        } catch (SocketTimeoutException e) {
            throw new TimeoutException("Connection or read timeout");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (printWriter != null)
                    printWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (bufferedReader != null)
                    bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return response;
    }

    /**
     * Connect to a web and get the content by HTTP POST Method
     *
     * @param strUrl            url of the website
     * @param itemList          the ArrayList of params you want to upload (*PostItem)
     * @param readTimeoutMillis the timeout value of reading
     * @return website content
     * @throws TimeoutException         HTTP connection or read timeout
     * @throws BadResponseCodeException HTTP response code is not 200
     */
    public static String httpPost(String strUrl, ArrayList<PostItem> itemList, int readTimeoutMillis)
            throws TimeoutException, BadResponseCodeException {

        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;
        String response = null;

        try {
            URL url = new URL(strUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.setRequestProperty("connection", "Keep-Alive");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("charset", "utf-8");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            httpURLConnection.setReadTimeout(readTimeoutMillis);

            String params = null;

            if (httpURLConnection.getResponseCode() != 200) {

                for (int i = 0; i < itemList.size(); i++) {
                    params += itemList.get(i).getKey() + "=" + itemList.get(i).getValue();
                    if (i < itemList.size() - 1) {
                        params += "&";
                    }
                }

                if (params != null && !params.trim().equals("")) {
                    printWriter = new PrintWriter(httpURLConnection.getOutputStream());
                    printWriter.print(params);
                    printWriter.flush();
                }

                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream
                        ()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }
            } else {
                throw new BadResponseCodeException(httpURLConnection.getResponseCode(), "ResponseCode is " +
                        "not 200");
            }
        } catch (SocketTimeoutException e) {
            throw new TimeoutException("Connection or read timeout");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (printWriter != null)
                    printWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (bufferedReader != null)
                    bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return response;
    }

    /**
     * Connect to a web and get the content by HTTP POST Method
     *
     * @param strUrl   url of the website
     * @param itemList the ArrayList of params you want to upload (*PostItem)
     * @return website content
     * @throws TimeoutException         HTTP connection or read timeout
     * @throws BadResponseCodeException HTTP response code is not 200
     */
    public static String httpPost(String strUrl, ArrayList<PostItem> itemList) throws TimeoutException,
            BadResponseCodeException {

        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;
        String response = null;

        try {
            URL url = new URL(strUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("accept", "*/*");
            httpURLConnection.setRequestProperty("connection", "Keep-Alive");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("charset", "utf-8");
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);

            String params = null;

            if (httpURLConnection.getResponseCode() != 200) {

                for (int i = 0; i < itemList.size(); i++) {
                    params += itemList.get(i).getKey() + "=" + itemList.get(i).getValue();
                    if (i < itemList.size() - 1) {
                        params += "&";
                    }
                }

                if (params != null && !params.trim().equals("")) {
                    printWriter = new PrintWriter(httpURLConnection.getOutputStream());
                    printWriter.print(params);
                    printWriter.flush();
                }

                bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream
                        ()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    response += line;
                }
            } else {
                throw new BadResponseCodeException(httpURLConnection.getResponseCode(), "ResponseCode is " +
                        "not 200");
            }
        } catch (SocketTimeoutException e) {
            throw new TimeoutException("Connection or read timeout");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (printWriter != null)
                    printWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (bufferedReader != null)
                    bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return response;
    }

    public class PostItem {

        private String key;
        private String value;

        public PostItem(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class BadResponseCodeException extends Exception {

        private int responseCode;

        public BadResponseCodeException(int responseCode) {
            super();
            this.responseCode = responseCode;
        }

        public BadResponseCodeException(int responseCode, String msg) {
            super(msg);
            this.responseCode = responseCode;
        }

        public int getResponseCode() {
            return this.responseCode;
        }
    }

    public static class TimeoutException extends Exception {

        public TimeoutException() {
            super();
        }

        public TimeoutException(String msg) {
            super(msg);
        }
    }

    public static String decodeShortenUrl(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(false);
        return httpURLConnection.getHeaderField("Location");
    }
}

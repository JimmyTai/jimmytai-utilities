package com.jimmytai.library.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;

/**
 * Created by JimmyTai on 15/11/21.
 */
public class ResourceUtils {

    public static int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT < 23)
            return ContextCompat.getColor(context, id);
        else
            return context.getResources().getColor(id, context.getTheme());
    }

    public static int getColor(Context context, int id, Resources.Theme style) {
        if (Build.VERSION.SDK_INT < 23)
            return ContextCompat.getColor(context, id);
        else
            return context.getResources().getColor(id, style);
    }

    public static String getString(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static Drawable getDrawable(Context context, int id) {
        if (Build.VERSION.SDK_INT < 23)
            return ContextCompat.getDrawable(context, id);
        else
            return context.getResources().getDrawable(id, context.getTheme());
    }

    public static Drawable getDrawable(Context context, int id, Resources.Theme style) {
        if (Build.VERSION.SDK_INT < 23)
            return ContextCompat.getDrawable(context, id);
        else
            return context.getResources().getDrawable(id, style);
    }

    public static XmlResourceParser getAnimation(Context context, int id) {
        return context.getResources().getAnimation(id);
    }
}

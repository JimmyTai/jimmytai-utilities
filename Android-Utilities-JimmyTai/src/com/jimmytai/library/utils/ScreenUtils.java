package com.jimmytai.library.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

public class ScreenUtils {

	public static final String TAG = "ScreenUtils";

	// Phone screen size less than 4.3
	public static final int PHONE_VGA_HDPI = 1;
	// Phone screen size larger than 4.3 and less than 6.3
	public static final int PHONE_720_XHDPI = 2;
	public static final int PHONE_720_XXHDPI = 3;
	public static final int PHONE_1080_XXHDPI = 4;

	// Tablet screen size lager than 6.3 and less than 9.5 --> 7 inch Tablet
	public static final int TABLET_720_MDPI = 5;
	public static final int TABLET_720_HDPI = 6;
	public static final int TABLET_1080_XHDPI = 7;
	public static final int TABLET_1080_XXHDPI = 8;

	// Tablet screen size lager than 9.5 --> 10 inch Tablet
	public static final int TABLET10_720_LDPI = 9;
	public static final int TABLET10_720_MDPI = 10;
	public static final int TABLET10_1080_HDPI = 11;
	public static final int TABLET10_1080_XHDPI = 12;

	/**
	 * Transfer unit sp to pixel
	 * 
	 * @param context
	 * @param spValue
	 *            sp
	 * @return
	 */
	public static float sp2px(Context context, float spValue) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return spValue * fontScale + 0.5f;
	}

	/**
	 * Transfer unit pixel to sp
	 * 
	 * @param context
	 * @param pxValue
	 *            pixel
	 * @return
	 */
	public static float px2sp(Context context, float pxValue) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return pxValue / fontScale + 0.5f;
	}

	/**
	 * Transfer unit dp to pixel
	 * 
	 * @param context
	 * @param dipValue
	 *            dp
	 * @return
	 */
	public static float dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return dipValue * scale + 0.5f;
	}

	/**
	 * Transfer unit pixel to dp
	 * 
	 * @param context
	 * @param pxValue
	 *            pixel
	 * @return
	 */
	public static float px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return pxValue / scale + 0.5f;
	}

	/**
	 * Get device width
	 * 
	 * @param context
	 * @return pixel of device width
	 */
	public static int getDeviceWidth(Context context) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics.widthPixels;
	}

	/**
	 * Get device height
	 * 
	 * @param context
	 * @return pixel of device height
	 */
	public static int getDeviceHeight(Context context) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics.heightPixels;
	}

	/**
	 * Get device density
	 * 
	 * @param context
	 * @return device density
	 */
	public static float getDeviceDensity(Context context) {

		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		return displayMetrics.density;
	}

	/**
	 * Get device resource density (drawable type)
	 * 
	 * @param context
	 * @return device resource density
	 */
	public static int getDeviceResourceDensity(Context context) {
		return context.getResources().getDisplayMetrics().densityDpi;
	}

	/**
	 * Get device size
	 * 
	 * @param context
	 * @return type of device size
	 */
	public static int getDeviceSize(Context context) {

		DisplayMetrics displaymetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displaymetrics);

		double screenWidth = displaymetrics.widthPixels;
		double screenHeight = displaymetrics.heightPixels;
		double diagonalPixels = Math.sqrt(Math.pow(screenWidth, 2)
				+ Math.pow(screenHeight, 2));
		float screenDensity = displaymetrics.density;
		double screenSize = diagonalPixels / (160 * screenDensity);
		int resourceDensity = context.getResources().getDisplayMetrics().densityDpi;

		if (3.5 < screenSize && screenSize <= 4.3) {
			Log.i(TAG, "Phone screen size is less than 4.3\"");
			if (480 <= screenWidth && screenWidth < 720) {
				Log.i(TAG, "Screen resolution: VGA");
				if ((DisplayMetrics.DENSITY_MEDIUM + DisplayMetrics.DENSITY_HIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_HIGH + DisplayMetrics.DENSITY_XHIGH) / 2) {
					Log.i(TAG, "Screen density: HDPI");
					return PHONE_VGA_HDPI;
				}
			}
		} else if (4.3 < screenSize && screenSize <= 6.5) {
			Log.i(TAG, "Phone screen size is between 4.3\" and 6.5\"");
			if (720 <= screenWidth && screenWidth < 1080) {
				Log.i(TAG, "Screen resolution: 720P");
				if ((DisplayMetrics.DENSITY_HIGH + DisplayMetrics.DENSITY_XHIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_XHIGH + DisplayMetrics.DENSITY_XXHIGH) / 2) {
					Log.i(TAG, "Screen density: XHDPI");
					return PHONE_720_XHDPI;
				} else if ((DisplayMetrics.DENSITY_XHIGH + DisplayMetrics.DENSITY_XXHIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_XXHIGH + DisplayMetrics.DENSITY_XXXHIGH) / 2) {
					Log.i(TAG, "Screen density: XXHDPI");
					return PHONE_720_XXHDPI;
				}
			} else if (1080 <= screenWidth && screenWidth < 2048) {
				Log.i(TAG, "Screen resolution: 1080P");
				if ((DisplayMetrics.DENSITY_XHIGH + DisplayMetrics.DENSITY_XXHIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_XXHIGH + DisplayMetrics.DENSITY_XXXHIGH) / 2) {
					Log.i(TAG, "Screen density: XXHDPI");
					return PHONE_1080_XXHDPI;
				}
			} else if (2048 <= screenWidth) {
				Log.i(TAG, "Screen resolution: 2K");
			}
		} else if (6.5 < screenSize && screenSize <= 9.5) {
			Log.i(TAG, "Tablet screen size is between 6.5\" and 9.5\"");
			if (720 <= screenWidth && screenWidth < 1080) {
				Log.i(TAG, "Screen resolution: 720P");
				if ((DisplayMetrics.DENSITY_LOW + DisplayMetrics.DENSITY_MEDIUM) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_MEDIUM + DisplayMetrics.DENSITY_HIGH) / 2) {
					Log.i(TAG, "Screen density: MDPI");
					return TABLET_720_MDPI;
				} else if ((DisplayMetrics.DENSITY_MEDIUM + DisplayMetrics.DENSITY_HIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_HIGH + DisplayMetrics.DENSITY_XHIGH) / 2) {
					Log.i(TAG, "Screen density: HDPI");
					return TABLET_720_HDPI;
				}
			} else if (1080 <= screenWidth && screenWidth < 2048) {
				Log.i(TAG, "Screen resolution: 1080P");
				if ((DisplayMetrics.DENSITY_HIGH + DisplayMetrics.DENSITY_XHIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_XHIGH + DisplayMetrics.DENSITY_XXHIGH) / 2) {
					Log.i(TAG, "Screen density: XHDPI");
					return TABLET_1080_XHDPI;
				} else if ((DisplayMetrics.DENSITY_XHIGH + DisplayMetrics.DENSITY_XXHIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_XXHIGH + DisplayMetrics.DENSITY_XXXHIGH) / 2) {
					Log.i(TAG, "Screen density: XXHDPI");
					return TABLET_1080_XXHDPI;
				}
			} else if (2048 <= screenWidth) {
				Log.i(TAG, "Screen resolution: 2K");
			}
		} else if (9.5 < screenSize) {
			Log.i(TAG, "Tablet screen size is lager than 9.5\"");
			if (720 <= screenWidth && screenWidth < 1080) {
				Log.i(TAG, "Screen resolution: HD");

				if (DisplayMetrics.DENSITY_LOW <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_LOW + DisplayMetrics.DENSITY_MEDIUM) / 2) {
					Log.i(TAG, "Screen density: LDPI");
					return TABLET10_720_LDPI;
				} else if ((DisplayMetrics.DENSITY_LOW + DisplayMetrics.DENSITY_MEDIUM) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_MEDIUM + DisplayMetrics.DENSITY_HIGH) / 2) {
					Log.i(TAG, "Screen density: MDPI");
					return TABLET10_720_MDPI;
				}
			} else if (1080 <= screenWidth && screenWidth < 2048) {
				Log.i(TAG, "Screen resolution: Full HD");
				if ((DisplayMetrics.DENSITY_MEDIUM + DisplayMetrics.DENSITY_HIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_HIGH + DisplayMetrics.DENSITY_XHIGH) / 2) {
					Log.i(TAG, "Screen density: HDPI");
					return TABLET10_1080_HDPI;
				} else if ((DisplayMetrics.DENSITY_HIGH + DisplayMetrics.DENSITY_XHIGH) / 2 <= resourceDensity
						&& resourceDensity < (DisplayMetrics.DENSITY_XHIGH + DisplayMetrics.DENSITY_XXHIGH) / 2) {
					Log.i(TAG, "Screen density: XHDPI");
					return TABLET10_1080_XHDPI;
				}
			} else if (2048 <= screenWidth) {
				Log.i(TAG, "Screen resolution: 2K");
			}
		}
		Log.i(TAG, "Not supported device");
		return 0;
	}
}
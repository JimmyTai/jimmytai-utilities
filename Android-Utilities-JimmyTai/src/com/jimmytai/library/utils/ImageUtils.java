package com.jimmytai.library.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

public class ImageUtils {

	public static final String TAG = "ImageUtils";

	public static Bitmap scaleImage(Context context, int id, float scale) {

		Log.i(TAG, "scaleImage");

		if (context == null)
			return null;

		Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), id);

		if (bmp == null)
			return null;

		int width = bmp.getWidth();
		int height = bmp.getHeight();

		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);
		Bitmap bmpNew = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix,
				true);

		return bmpNew;
	}
}

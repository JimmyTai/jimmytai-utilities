package com.jimmytai.library.log;

import android.util.Log;

/**
 * Created by JimmyTai on 15/10/13.
 */
public class JLog {

    // ---------- i ------------
    public static void i(boolean isDebug, String tag, String content) {
        if (isDebug)
            Log.i(tag, content);
    }

    public static void i(boolean isDebug, String tag, String content, Throwable tr) {
        if (isDebug)
            Log.i(tag, content, tr);
    }

    // ---------- d ------------
    public static void d(boolean isDebug, String tag, String content) {
        if (isDebug)
            Log.d(tag, content);
    }

    public static void d(boolean isDebug, String tag, String content, Throwable tr) {
        if (isDebug)
            Log.d(tag, content, tr);
    }

    // ---------- e ------------
    public static void e(boolean isDebug, String tag, String content) {
        if (isDebug)
            Log.e(tag, content);
    }

    public static void e(boolean isDebug, String tag, String content, Throwable tr) {
        if (isDebug)
            Log.e(tag, content, tr);
    }

    // ---------- w ------------
    public static void w(boolean isDebug, String tag, String content) {
        if (isDebug)
            Log.w(tag, content);
    }

    public static void w(boolean isDebug, String tag, String content, Throwable tr) {
        if (isDebug)
            Log.w(tag, content, tr);
    }

    // ---------- v ------------
    public static void v(boolean isDebug, String tag, String content) {
        if (isDebug)
            Log.v(tag, content);
    }

    public static void v(boolean isDebug, String tag, String content, Throwable tr) {
        if (isDebug)
            Log.v(tag, content, tr);
    }

    // ---------- wtf ------------
    public static void wtf(boolean isDebug, String tag, String content) {
        if (isDebug)
            Log.wtf(tag, content);
    }

    public static void wtf(boolean isDebug, String tag, String content, Throwable tr) {
        if (isDebug)
            Log.wtf(tag, content, tr);
    }
}

package com.jimmytai.library.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jimmytai.library.log.JLog;

/**
 * Created by JimmyTai on 15/12/10.
 */
public abstract class JFragment extends Fragment {

    public Activity jActivity;
    public View jView;

    public abstract String setTag();

    public abstract boolean setDebug();

    public abstract int setLayout();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        JLog.i(setDebug(), setTag(), "onAttach() ==> api >= 23");
        if (Build.VERSION.SDK_INT >= 23) {
            if (context instanceof Activity) {
                this.jActivity = (Activity) context;
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        JLog.i(setDebug(), setTag(), "onAttach() ==> api < 23");
        if (Build.VERSION.SDK_INT < 23) {
            this.jActivity = activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JLog.i(setDebug(), setTag(), "onCreate()");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        JLog.i(setDebug(), setTag(), "onCreateView()");
        jView = inflater.inflate(setLayout(), container, false);
        return jView;
    }

    @Override
    public void onStart() {
        super.onStart();
        JLog.i(setDebug(), setTag(), "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        JLog.i(setDebug(), setTag(), "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        JLog.i(setDebug(), setTag(), "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        JLog.i(setDebug(), setTag(), "onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        JLog.i(setDebug(), setTag(), "onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        JLog.i(setDebug(), setTag(), "onDestroy()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        JLog.i(setDebug(), setTag(), "onStart()");
    }
}

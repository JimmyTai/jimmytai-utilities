package com.jimmytai.library.activity;

import android.app.Activity;
import android.os.Bundle;

import com.jimmytai.library.log.JLog;

/**
 * Created by JimmyTai on 15/11/26.
 */
public abstract class JActivity extends Activity {

    public abstract String setTag();

    public abstract boolean setDebug();

    public abstract int setLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JLog.i(setDebug(), setTag(), "onCreate()");
        setContentView(setLayout());
    }

    @Override
    protected void onStart() {
        super.onStart();
        JLog.i(setDebug(), setTag(), "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        JLog.i(setDebug(), setTag(), "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        JLog.i(setDebug(), setTag(), "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        JLog.i(setDebug(), setTag(), "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        JLog.i(setDebug(), setTag(), "onDestroy()");
    }
}
